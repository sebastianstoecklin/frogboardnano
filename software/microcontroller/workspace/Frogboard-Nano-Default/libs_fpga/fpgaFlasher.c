/***************************************************************************//**
 * @file    fpgaFlasher.c
 * @author  Sebastian Stoecklin
 * @date    05/06/2019
 *
 * @brief   A basic library to flash an iCE40 device using SPI.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 * 			The library assumes that USART module as well
 *      	as the I/Os have successfully been configured.
 ******************************************************************************/

#include "fpgaFlasher.h"
#include "led.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
void simpleDelay(int delayMS);

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/
void simpleDelay(int delayMS) {
	long tmp = 0;
	for (tmp = 0; tmp < delayMS * 4000; tmp++)
		;
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/
void fpga_reset(void) {
	/* Init FPGA pins */
	GPIO_PinModeSet(FPGA_CRESET_PORT, FPGA_CRESET_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(FPGA_CS_PORT, FPGA_CS_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(FPGA_CDONE_PORT, FPGA_CDONE_PIN, gpioModeInputPull, 1);

	/* Keep FPGA in reset state: */
	GPIO_PinOutClear(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
}

/* resets the internal logic using CDONE as reset pin */
void fpga_logicReset(void) {
	/* init pin as �C output set to 0 */
	GPIO_PinModeSet(FPGA_CDONE_PORT, FPGA_CDONE_PIN, gpioModePushPull, 0);
	simpleDelay(10);
	/* reconfigure pin as �C input, FPGA pullup will pull it back to high */
	GPIO_PinModeSet(FPGA_CDONE_PORT, FPGA_CDONE_PIN, gpioModeInputPull, 1);
}

void fpga_flash(const unsigned char * binary, unsigned int binary_len) {

	/* set CRESET high for starting the sequence
	 * while keeping CS low
	 * see iCE40 Programming and Configuration, page 4 */
	GPIO_PinOutClear(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
	GPIO_PinOutClear(FPGA_CS_PORT, FPGA_CS_PIN);
	simpleDelay(1);
	GPIO_PinOutSet(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
	simpleDelay(2);

	/* write the data using SPI */
	long cnt = 0;
	for(cnt = 0; cnt < binary_len; cnt++) {
		USART_SpiTransfer(SPI_USART, binary[cnt]);
	}

	while(!GPIO_PinInGet(FPGA_CDONE_PORT,FPGA_CDONE_PIN));

	/* clock at least for another 49 times to release all GPIO *
	 * here: 7 * 8 bits = 56 clocks */
	for(cnt = 0; cnt < 8; cnt++) {
		USART_SpiTransfer(SPI_USART, 0x00);
	}

	/* reset chip select */
	GPIO_PinOutSet(FPGA_CS_PORT, FPGA_CS_PIN);

	/* perform reset of internal custom FPGA logic */
	fpga_logicReset();

}
