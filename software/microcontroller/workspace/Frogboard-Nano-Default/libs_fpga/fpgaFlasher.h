/***************************************************************************//**
 * @file    fpgaFlasher.h
 * @author  Sebastian Stoecklin
 * @date    04/11/2017
 *
 * @brief   A basic library to flash an iCE40 device using SPI.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 * 			The library assumes that USART module as well
 *      	as the I/Os have successfully been configured.
 ******************************************************************************/

#ifndef INC_FPGAFLASHER_H_
#define INC_FPGAFLASHER_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "spi.h"
#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"

/* Include file for pin definitions: */

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define SPI_USART	USART1

#define FPGA_CRESET_PORT 	gpioPortD
#define FPGA_CRESET_PIN		13
#define FPGA_CS_PORT		gpioPortC
#define FPGA_CS_PIN			10
#define FPGA_CDONE_PORT		gpioPortD
#define FPGA_CDONE_PIN		15

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void fpga_reset(void);
extern void fpga_logicReset(void);
extern void fpga_flash(const unsigned char * binary, unsigned int binary_len);

#endif /* INC_FPGA-FLASHER_H_ */
