/***************************************************************************//**
 * @file    radio.h
 * @author  Sebastian Stoecklin
 * @date    03/07/2017
 *
 * @brief   A simple library to access the RAIL library for EFR32 devices.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef INC_RADIO_H_
#define INC_RADIO_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "config.h"

#include "led.h"
#include "serial.h"
#include "timer.h"
#include "rail_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rail.h"
#include "rail_types.h"
#include "em_device.h"
#include "em_gpio.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define MAX_FRAME_LENGTH		(4093)
#define PACKET_HEADER_LEN		(2)

#define TX_FIFO_SIZE 			(512)
#define TX_FIFO_THRESHOLD		(40)
#define RX_FIFO_THRESHOLD		(100)

#define HAL_PA_ENABLE			(1)

#define RF_PA_RAMP				(10)
#define RF_PA_2P4_LOWPOWER		(0)
#define RF_PA_POWER				(252)
#define RF_PA_CURVE_HEADER		"pa_curves_efr32.h"


/* type definition for radio state variable: */
typedef enum {
	undefined,
	idle,
	listen,
	sleep,
	rx_inProgress,
	rx_error,
	rx_successful,
	tx_inProgress,
	tx_error,
	tx_successful,
} Radio_State_t;

/* type definition for radio handle variable: */
typedef struct Radio_Handle {
	Radio_State_t state;
	uint8_t channel;
	int16_t rssi;

	/* pointer to current read/write buffers */
	uint8_t * txBufPtr;
	uint8_t * rxBufPtr;

	/* current data length */
	uint16_t txLength;
	uint16_t txWritten;
	uint16_t rxReceived;
	uint16_t rxExpected;
	uint8_t  fifoReadCnt;

} Radio_Handle_t;


/******************************************************************************
 * VARIABLES
 *****************************************************************************/



/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/
void RAILCb_Generic(RAIL_Handle_t railHandle, RAIL_Events_t events);
void storeReceivedPackets(uint16_t);
void printReceivedPackets();

void radio_init(void);
void radio_tx(uint8_t *, uint16_t, void *);
void radio_startRx(uint8_t *, void *);
uint16_t radio_getRxLength(void);
int16_t radio_getRssi(void);
Radio_State_t radio_getState(void);


#endif /* INC_RADIO_H_ */
