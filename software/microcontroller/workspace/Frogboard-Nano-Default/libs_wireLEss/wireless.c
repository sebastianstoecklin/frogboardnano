/***************************************************************************//**
 * @file    wireless.c
 * @author  Sebastian Stoecklin
 * @date    01/11/2018
 *
 * @brief   WireLEss - a communication library for the EFR32 radio
 * 			with buffered data handler.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "wireless.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

volatile uint8_t radioTxBuffer[RADIO_BUFFER_CNT][RADIO_BUFFER_LENGTH];
volatile uint16_t radioTxBufferLength[RADIO_BUFFER_CNT] = { 0, 0, 0, 0 };
volatile uint8_t freeTxSlots = RADIO_BUFFER_CNT;// gives the number of available buffers, including the one currently being written to
volatile uint8_t currentTxBufferIndex = 0; 	// index
volatile uint8_t nextTxIndex = 0;			// index

volatile uint8_t radioRxBuffer[RADIO_BUFFER_CNT][RADIO_BUFFER_LENGTH];
volatile uint16_t radioRxBufferLength[RADIO_BUFFER_CNT] = { 0, 0, 0, 0 };
volatile uint8_t freeRxSlots = RADIO_BUFFER_CNT; // number of buffers available, including the current one
volatile uint8_t currentRxBufferIndex = 0;	// index
volatile uint8_t nextRxIndex = 0;			// index

void (*radioRxUserCallback)(void);

volatile WireLEss_Handle_t wireless = { .txBuffer = &radioTxBuffer[0][2],// start with byte 2 so that the first two bytes can be rewritten
																		 //by the radio_tx function to include the length
		.rxBuffer = &radioRxBuffer[0][2], .rxDataCnt = 0,

		.rxDataAvailable = 0, };

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

/* Init functions */

/* Transmit functions */

void wireless_init(void * userCallback) {
	if (radio_getState() == undefined) {
		radio_init();
	} else if (radio_getState() == idle) {
		wireless_startRx();
		wireless_attachRxCallback(userCallback);
	}
}

void wireless_scheduleTx(uint16_t length) {

	while (freeTxSlots == 0) {
		serial_write('!');
	}

	// store the length
	radioTxBufferLength[currentTxBufferIndex] = length + 2;

	/* select next buffer: increase, but keep in valid range */
	currentTxBufferIndex = (currentTxBufferIndex + 1) % RADIO_BUFFER_CNT;
	wireless.txBuffer = &radioTxBuffer[currentTxBufferIndex][2];
	freeTxSlots--; // decrease number of available buffer slots (increase by one on successful transmit)

	if (((currentTxBufferIndex - 1) == nextTxIndex)
			|| ((nextTxIndex == RADIO_BUFFER_CNT - 1)
					&& (currentTxBufferIndex == 0))) {
		/* the currently scheduled packet is the next to be transmitted, so do it: */
		radio_tx(radioTxBuffer[nextTxIndex], radioTxBufferLength[nextTxIndex],
				wireless_TxCallback);
	} else {
		/* ongoing transmission, don't do anything */
	}
}

void wireless_TxCallback(void) {
	if (radio_getState() == tx_successful) {
		// everything went fine
		freeTxSlots++; // one additional memory slot is available
		nextTxIndex = (nextTxIndex + 1) % RADIO_BUFFER_CNT; // increase the to-be-transmitted index

		if (freeTxSlots == RADIO_BUFFER_CNT) {
			// no further transmissions scheduled, go to listen:
			wireless_startRx();
		} else {
			// transmit next data packet:
			radio_tx(radioTxBuffer[nextTxIndex],
					radioTxBufferLength[nextTxIndex], wireless_TxCallback);
		}
	} else {
		// error, to be handled in future (TODO)
		while (1)
			;
	}
}

/* Receive functions */

void wireless_startRx() {
	radio_startRx(radioRxBuffer[currentRxBufferIndex], wireless_RxCallback);
}

void wireless_attachRxCallback(void * function) {
	radioRxUserCallback = function;
}

void wireless_nextRxBuffer(void) {
	if (nextRxIndex != currentRxBufferIndex) {
		/* if the next to be read and the current rx buffer are not the same (i.e. there was really something to read out) */
		nextRxIndex = (nextRxIndex + 1) % RADIO_BUFFER_CNT;
		freeRxSlots++;
		wireless.rxBuffer = &radioRxBuffer[nextRxIndex][2];

		if (freeRxSlots != RADIO_BUFFER_CNT) {
			/* There are still more packets to be read */
			wireless.rxDataAvailable = 1;
			wireless.rxDataCnt = radioRxBufferLength[nextRxIndex];
		} else {
			/* No additional packets: */
			wireless.rxDataAvailable = 0;
			wireless.rxDataCnt = 0;
		}
	}
}

void wireless_RxCallback(void) {
	if (radio_getState() == rx_successful) {
		/* everything went fine */
		freeRxSlots--;
		radioRxBufferLength[currentRxBufferIndex] = radio_getRxLength() - 2;

		if (freeRxSlots != 0) {
			// there are still empty buffers: route to next buffer location and start receiving
			currentRxBufferIndex =
					(currentRxBufferIndex + 1) % RADIO_BUFFER_CNT;
			wireless_startRx();
		} else {
			// all buffers full, don't go to listening mode
			while (1)
				;
		}
	} else {
		// error condition:
		wireless_startRx();
	}

	if (wireless.rxDataAvailable == 0) {
		wireless.rxDataAvailable = 1;
		wireless.rxDataCnt = radioRxBufferLength[nextRxIndex];
		radioRxUserCallback();
	}
}

/******************************************************************************
 * Takes the current receiving buffer and turns it into a WireLEss message.
 * The RX buffer is still maintained (message.data is just a pointer the data).
 ******************************************************************************/
void wireless_receiveMessage(WireLEss_Message_t * message) {
	if (wireless.rxBuffer[0] == '@' && wireless.rxBuffer[3] == '?') {
		// valid message
		(*message).recipient = (wireless.rxBuffer[1] << 8) | (wireless.rxBuffer[2] << 0);
		(*message).originator = (wireless.rxBuffer[4] << 8) | (wireless.rxBuffer[5] << 0);
		(*message).type = wireless.rxBuffer[6];
		(*message).length = ((wireless.rxBuffer[-2] << 8) | (wireless.rxBuffer[-1] << 0)) - 7;
		(*message).data = &wireless.rxBuffer[7];
	} else {
		(*message).recipient  = 0;
		(*message).originator = 0;
	}
}

/******************************************************************************
 * Builds a TX string out of a message.
 * The TX event will automatically be initiated.
 ******************************************************************************/
void wireless_transmitMessage(WireLEss_Message_t * message) {
	uint32_t packetLength = 0;
	int cnt = 0;

	wireless.txBuffer[packetLength++] = '@';
	wireless.txBuffer[packetLength++] = ((*message).recipient >> 8) & 0xFF;
	wireless.txBuffer[packetLength++] = ((*message).recipient >> 0) & 0xFF;

	wireless.txBuffer[packetLength++] = '?';
	wireless.txBuffer[packetLength++] = ((*message).originator >> 8) & 0xFF;
	wireless.txBuffer[packetLength++] = ((*message).originator >> 0) & 0xFF;

	wireless.txBuffer[packetLength++] = (*message).type;
	for(cnt = 0; cnt < (*message).length; cnt++) {
		wireless.txBuffer[packetLength++] = (*message).data[cnt];
	}

	wireless_scheduleTx(packetLength);
}
