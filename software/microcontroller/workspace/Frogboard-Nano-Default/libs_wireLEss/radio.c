/***************************************************************************//**
 * @file    radio.c
 * @author  Sebastian Stoecklin
 * @date    03/07/2017
 *
 * @brief   A simple library to access the RAIL library for EFR32 devices.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "radio.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

Radio_Handle_t radio = {
	.state		= undefined,
	.channel	= 0,
};

void (*txCompletedCallback)(void) = NULL;
void (*rxCompletedCallback)(void) = NULL;

/* RAIL specific requirements */
uint8_t txFifo[TX_FIFO_SIZE];
RAIL_Handle_t railHandle;
static RAIL_Config_t railCfg = { .eventsCallback = &RAILCb_Generic, };
static const RAIL_DataConfig_t dataConfig =
		{ .txSource = TX_PACKET_DATA, .rxSource = RX_PACKET_DATA, .txMethod =
				FIFO_MODE, .rxMethod = FIFO_MODE, };

volatile uint32_t locked = 1;

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/
void storeReceivedPackets(uint16_t bytesAvailable) {
	if (radio.rxExpected == 0) {
		radio.rxReceived = RAIL_ReadRxFifo(railHandle, radio.rxBufPtr, bytesAvailable);
		radio.rxExpected = (radio.rxBufPtr[0] << 8) + radio.rxBufPtr[1] + PACKET_HEADER_LEN; //received length + length of header
	} else {
		uint16_t _rxReceived = radio.rxReceived;
		uint16_t _rxExpected = radio.rxExpected;
		radio.rxReceived += RAIL_ReadRxFifo(railHandle, radio.rxBufPtr + _rxReceived,
		        _rxExpected - _rxReceived);
	}
}

/******************************************************************************
 * RAIL Callback Implementation
 *****************************************************************************/
void RAILCb_Generic(RAIL_Handle_t railHandle, RAIL_Events_t events) {
	if (events & RAIL_EVENT_TX_PACKET_SENT) {
		/* TX event successfully completed */
		serial_debug_printLn("RAIL_EVENT_TX_PACKET_SENT");
		GPIO_PinOutClear(gpioPortC, 11);
		radio.state = tx_successful;
		if(txCompletedCallback != NULL) {
			txCompletedCallback();
		}
	}
	if (events & RAIL_EVENT_TX_UNDERFLOW) {
		/* TX FIFO was underflowing, start to receive again */
		radio.state = tx_error;
		serial_debug_printLn("RAIL_EVENT_TX_UNDERFLOW");
		if(txCompletedCallback != NULL) {
			txCompletedCallback();
		}
	}
	if (events & RAIL_EVENT_TX_FIFO_ALMOST_EMPTY) {
		/* TX FIFO needs additional bytes */
		if (radio.txLength - radio.txWritten > 0) {
			radio.txWritten += RAIL_WriteTxFifo(railHandle, radio.txBufPtr + radio.txWritten,
					radio.txLength - radio.txWritten,
					false);
		}
		serial_debug_printLn("RAIL_EVENT_TX_FIFO_ALMOST_EMPTY");
		radio.state = tx_inProgress;
	}
	if (events & RAIL_EVENT_RX_SYNC1_DETECT) {
		//serial_debug_printLn("RAIL_EVENT_RX_SYNC1_DETECT");
		radio.rssi = RAIL_GetRssi(railHandle, true);
		radio.state = rx_inProgress;
	}
	if (events & RAIL_EVENT_RX_PACKET_RECEIVED) {
		/* RX event successfully completed */
		//serial_debug_printLn("RAIL_EVENT_RX_PACKET_RECEIVED");
		storeReceivedPackets(RAIL_GetRxFifoBytesAvailable(railHandle));
		radio.state = rx_successful;
		if(rxCompletedCallback != NULL) {
			rxCompletedCallback();
		}
	}
	if (events & RAIL_EVENT_RX_FIFO_ALMOST_FULL) {
		/* RX FIFO almost full, read out some bytes to free memory */
		storeReceivedPackets(RAIL_GetRxFifoBytesAvailable(railHandle));
		radio.state = rx_inProgress;
	}
	if (events & RAIL_EVENT_RX_FIFO_OVERFLOW) {
		//serial_debug_printLn("RAIL_EVENT_RX_FIFO_OVERFLOW");
		radio.state = rx_error;
		if(rxCompletedCallback != NULL) {
			rxCompletedCallback();
		}
	}
	if (events & RAIL_EVENT_RX_PACKET_ABORTED) {
		radio.state = rx_error;
		if(rxCompletedCallback != NULL) {
			rxCompletedCallback();
		}
	}
	if (events & RAIL_EVENT_RX_FRAME_ERROR) {
		radio.state = rx_error;
		if(rxCompletedCallback != NULL) {
			rxCompletedCallback();
		}
	}
	if (events & RAIL_EVENT_RX_PREAMBLE_DETECT) {
		//serial_debug_printLn("RAIL_EVENT_RX_PREAMBLE_DETECT");
		radio.state = rx_inProgress;
	}
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void radio_init(void) {
	railHandle = RAIL_Init(&railCfg, NULL);
	if (railHandle == NULL) {
		while (1)
			;
	}
	RAIL_ConfigCal(railHandle, RAIL_CAL_ALL);

	// Set us to a valid channel for this config and force an update in the main
	// loop to restart whatever action was going on
	RAIL_ConfigChannels(railHandle, channelConfigs[0], NULL);

	// Enable FIFO mode
	RAIL_ConfigData(railHandle, &dataConfig);

	// Configure RAIL callbacks, with buffer error callbacks
	RAIL_ConfigEvents(railHandle, RAIL_EVENTS_ALL, (
					  RAIL_EVENT_TX_UNDERFLOW
					| RAIL_EVENT_TX_FIFO_ALMOST_EMPTY
					| RAIL_EVENT_TX_PACKET_SENT
					| RAIL_EVENT_RX_SYNC1_DETECT
					| RAIL_EVENT_RX_FIFO_ALMOST_FULL
					| RAIL_EVENT_RX_PACKET_ABORTED
				    | RAIL_EVENT_RX_FRAME_ERROR
				    | RAIL_EVENT_RX_FIFO_OVERFLOW
					| RAIL_EVENT_RX_PACKET_RECEIVED
#if RAIL_PREAMBLE_INT == TRUE
					| RAIL_EVENT_RX_PREAMBLE_DETECT
#endif
			));

	// Set TX FIFO
	uint16_t fifoSize = RAIL_SetTxFifo(railHandle, txFifo, 0, TX_FIFO_SIZE);

	if (fifoSize != TX_FIFO_SIZE) {
		while (1)
			;
	}

	// Set FIFO thresholds
	RAIL_SetRxFifoThreshold(railHandle, RX_FIFO_THRESHOLD); //FIFO size is 512B
	RAIL_SetTxFifoThreshold(railHandle, TX_FIFO_THRESHOLD);

	// Initialize the PA now that the HFXO is up and the timing is correct
	RAIL_TxPowerConfig_t txPowerConfig = {
			.mode = RAIL_TX_POWER_MODE_2P4_LP, // RAIL_TX_POWER_MODE_2P4_LP exists for low-power
			.voltage = RF_PA_VOLTAGE,
			.rampTime = RF_PA_RAMP,
	};

	if (channelConfigs[0]->configs[0].baseFrequency < 1000000UL) {
		// Use the Sub-GHz PA if required
		txPowerConfig.mode = RAIL_TX_POWER_MODE_SUBGIG;
	}

	if (RAIL_ConfigTxPower(railHandle, &txPowerConfig)
			!= RAIL_STATUS_NO_ERROR) {
		// Error: The PA could not be initialized due to an improper configuration.
		// Please ensure your configuration is valid for the selected part.
		while (1);
	}

	RAIL_SetTxPower(railHandle, 7);
	RAIL_Idle(railHandle, RAIL_IDLE, true);
	radio.state = idle;
}

void radio_tx(uint8_t * data, uint16_t length, void * txCompletedUserCallback) {
	radio.txLength = length;
	radio.txBufPtr = data;

	RAIL_Idle(railHandle, RAIL_IDLE, true);

	// Write packet length to first two bytes of the packet:
	radio.txBufPtr[0] = (uint8_t) ((radio.txLength - PACKET_HEADER_LEN) >> 8) & 0xFF;
	radio.txBufPtr[1] = (uint8_t) (radio.txLength - PACKET_HEADER_LEN) & 0x00FF;

	/* Set callback, write to FIFO and start transmitting */
	txCompletedCallback = txCompletedUserCallback; // if NULL, it is not processed
	radio.txWritten = RAIL_WriteTxFifo(railHandle, radio.txBufPtr, radio.txLength, true);

	RAIL_StartTx(railHandle, radio.channel, RAIL_TX_OPTIONS_DEFAULT, NULL);
	radio.state = tx_inProgress;
}

void radio_startRx(uint8_t * rxBuffer, void * rxCompletedUserCallback) {
	radio.rxBufPtr = rxBuffer;
	RAIL_Idle(railHandle, RAIL_IDLE, true);
	RAIL_ResetFifo(railHandle, false, true);
	radio.rxReceived = 0;
	radio.fifoReadCnt = 0;
	radio.rxExpected = 0;

	/* set callback and start listening mode */
	rxCompletedCallback = rxCompletedUserCallback;
	RAIL_StartRx(railHandle, radio.channel, NULL);
	radio.state = listen;

	/* set callback and start listening mode */
//	rxCompletedCallback = rxCompletedUserCallback;
//	RAIL_StartRx(railHandle, radio.channel, NULL);
//	radio.state = listen;

//	RAIL_Idle(railHandle, RAIL_IDLE, true);
//	radio.state = idle;
}

uint16_t radio_getRxLength(void) {
	return radio.rxReceived;
}

int16_t radio_getRssi(void) {
	return radio.rssi;
}

Radio_State_t radio_getState(void) {
	return radio.state;
}
