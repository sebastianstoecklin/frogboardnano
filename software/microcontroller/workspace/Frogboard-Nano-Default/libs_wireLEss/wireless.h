/***************************************************************************//**
 * @file    wireless.h
 * @author  Sebastian Stoecklin
 * @date    01/11/2018
 *
 * @brief   WireLEss - a communication library for the EFR32 radio
 * 			with buffered data handler.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 *
 * @details
 *
 *   1. Prerequisites:
 *    - Make sure your relevant files include wireless.h.
 *    - Call the function wireless_init(void * userCallback) to boot the radio and
 *      to start listening.
 *      Be aware that there are no low-power features at the moment!
 *    - If your radio starts to receive packets after all previous packets
 *      have been read, it will start a user-defined callback function.
 *      You are free to design that function and to name it however you want.
 *      When initializing Rx, you can hand over this function to your library
 *      using the variable "userCallback". Alternatively, you can change this
 *      callback any time by executing wireless_attachRxCallback(userCallback),
 *      with userCallback being the name of your custom callback function.
 *      Please just use it for setting flags, as it will block the receiver
 *      interface otherwise. Be aware that this function is called from a
 *      radio ISR with highest priority, i.e. no other interrupts occur
 *      during the execution of this user function.
 *
 *      An example of getting started:
 *
 *      	wireless_init(app_wirelessTerminalRxCb);
 *
 *
 *   2. Transmitting (TX):
 *   Always write your data (uint8_t, i.e. 8 bit chars) to the
 *   pointer wireless.txBuffer (max. 2048 Bytes), e.g. by
 *
 *   		wireless.txBuffer[0] = 0;
 *   		wireless.txBuffer[1] = 0xAA;
 *
 *   Start your transmission by calling
 *
 *   		radio_scheduleTx(length);
 *
 *   with "length" being the number of bytes to trasmit from the
 *   given array wireless.txBuffer. You can immediately write to the array
 *   again and don't have to wait for TX completion.
 *   Attention: Writing more than 2048 bytes might result in writing to unknown
 *   memory cells. Please be sure about what you do (no safety features atm).
 *
 *   3. Receiving (RX):
 *   When previous receive events have been completed, i.e. no more data
 *   in the receive buffers, you will get notified by an occuring RX event
 *   by the execution of your callback function userCallback you specified in
 *   step 1.
 *   After that, you get access to the received data by using the pointer
 *   wireless.rxBuffer, i.e. you can access your data like that:
 *
 *   	uint8_t temp = wireless.rxBuffer[0];
 *   	temp = wireless.rxBuffer[1];
 *
 *   The length of the received data can be accessed by the variable
 *   wireless.rxDataCnt.
 *   If you processed or stored the received data, you can release the allocated
 *   memory by calling the function
 *
 *   	wireless_nextRxBuffer();
 *
 *	 Be aware that the data can then be overwritten by the radio again.
 *   Calling wireless_nextRxBuffer(); will also move the pointer wireless.rxBuffer,
 *   so that you can't access the data anymore on an easy way.
 *   The presence of subsequent radio packets must be checked by the status variable
 *   wireless.rxDataAvailable. This can also be read any time in your program to check
 *   for new packets, thus avoiding to rely on your callback. Please also note that the
 *   callback is just executed for new data streams with empty buffer in the beginning,
 *   so that a quick reception of a second packet won't trigger the interrupt any more.
 *
 * Hints:
 *
 * - Avoid fast sequences of small packets. Its very likely that the reception
 *   process is not fast enough to follow, so that some might be dropped.
 *   Either add a delay before scheduling a new TX or some larger packets
 *   (storing the data in a fast is less constraint, as long as you are
 *   able to process them).
 *
 * - Debugging with radio can have a serious effect on the packet loss,
 *   especially when handling fast transmissions. Entering a breakpoint
 *   might avoid a RAIL callback to be processed, so that the FIFO cannot be
 *   filled or read out fast enough, resulting in a corrupt packet.
 *
 * - If your radio does not work properly, reasons might be:
 *    - a bad oscillator (not stable, frequency offset)
 *    - a bad antenna matching (reflections might lead to inter-symbol interference)
 *    - a bad power supply (noise and ripples destoy range)
 *    - a bad antenna (a small antenna ;-) )
 *    - the programmer of this library was lacking coffee
 ******************************************************************************/

#ifndef LIBRARIES_WIRELESSCOMMUNICATION_H_
#define LIBRARIES_WIRELESSCOMMUNICATION_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "led.h"
#include "serial.h"
#include "timer.h"
#include "radio.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/
#define RADIO_BUFFER_CNT	4
#define RADIO_BUFFER_LENGTH	2050 // 2048 data bytes + 2 length bytes

/* type definition for radio handle variable: */
typedef struct WireLEss_Handle {

	uint8_t * txBuffer;
	uint8_t * rxBuffer;
	uint8_t rxDataAvailable;
	uint16_t rxDataCnt;

} WireLEss_Handle_t;

/*********************************************************************
 * A typedef for a wireless message: @aa?bbCdddd
 *
 *  aa   is HEX ID of the receiver device (this one)
 *  bb   is the ASCII encoded HEX ID of the transmitter device
 *  C    encodes the message type, here: B for binary
 *  dddd data of arbitrary length
 *
 * example: @12?42Bddee
 *********************************************************************/
typedef struct WireLEss_Message {

	uint16_t originator;
	uint16_t recipient;
	uint8_t  type;
	uint32_t length;
	uint8_t * data;

} WireLEss_Message_t;

/******************************************************************************
 * VARIABLES
 *****************************************************************************/


/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/
void wireless_init(void *);

void wireless_scheduleTx(uint16_t);
void wireless_TxCallback(void);

void wireless_startRx(void);
void wireless_attachRxCallback(void *);
void wireless_nextRxBuffer(void);
void wireless_RxCallback(void);

void wireless_receiveMessage(WireLEss_Message_t *);
void wireless_transmitMessage(WireLEss_Message_t *);


#endif /* LIBRARIES_WIRELESSCOMMUNICATION_H_ */
