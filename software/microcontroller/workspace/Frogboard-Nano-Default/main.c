/***************************************************************************//**
 * @file    main.h
 * @author  Sebastian Stoecklin
 * @date    2019/05/13
 *
 * @brief   A basic program demonstrating the basic features
 * 			of the Frogboard Nano V1.
 *
 * @note	The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

/* configuration */
#include "config.h"

/* peripheral drivers */
#include "gpioInterrupt.h"
#include "adc.h"
#include "led.h"
#include "serial.h"
#include "spi.h"
#include "timer.h"
#include "peripherals.h"

/* applications */
#include "fpgaFlasher.h"
#include "commands.h"
#include "apps.h"

/* FPGA config to program */
#include "fpgaBinaries.h"

/* utility prototypes */
void controller_init(void);
void timer_callback(void);

int main(void) {
  controller_init();
  radio_init();

  led_on();
  wait_ms(500);
  led_off();

  /* Infinite loop */
  while (1) {
	  //app_wirelessTerminal();
	  //app_echo();
  }
}

/******************************************************************************
 * SUBROUTINES AND CALLBACKS
 *****************************************************************************/

void controller_init(void) {
	/* start chip */
	CHIP_Init();

	/*** init typical internal modules: ***/
	dcdc_init();
	oscillators_init();
	serial_init();
	spi_init();
	timer_init(TICK_TIME_US, timer_callback);	// call periodic function

	/*** init board specific modules: ***/
	fpga_reset();
	fpga_flash(fpgaBinaries_bin, fpgaBinaries_bin_len); // boot FPGA
	//adc_single_init();
	osc_enable();
	led_init(STATUS_LED_PORT, STATUS_LED_PIN);

}

void timer_callback(void) {
	/* a periodic callback function */

}

