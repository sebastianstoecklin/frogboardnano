/***************************************************************************//**
 * @file    spi.c
 * @author  Sebastian Stoecklin
 * @date    10/12/2018
 *
 * @brief   A basic library setting up the SPI module.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 *
 ******************************************************************************/

#include "spi.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/


/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/


/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/


/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void spi_init(void) {

	/*****************************************
	 * Setup of hardware module
	 ****************************************/

	/** general init **/
	USART_InitSync_TypeDef initsync = USART_INITSYNC_DEFAULT;
	initsync.enable                = usartDisable;
	initsync.baudrate              = SPI_CLK;
	initsync.databits              = usartDatabits8;
	initsync.master                = 1;
	initsync.msbf                  = 1;
	initsync.clockMode             = usartClockMode3;
	#if defined( USART_INPUT_RXPRS ) && defined( USART_TRIGCTRL_AUTOTXTEN )
	initsync.prsRxEnable           = 0;
	initsync.prsRxCh               = 0;
	initsync.autoTx                = 0;
	#endif
	USART_InitSync(SPI_USART, &initsync);

	/** PRS init **/
	USART_PrsTriggerInit_TypeDef initprs = USART_INITPRSTRIGGER_DEFAULT;
	initprs.rxTriggerEnable        = 0;
	initprs.txTriggerEnable        = 0;
	initprs.prsTriggerChannel      = usartPrsTriggerCh0;
	USART_InitPrsTrigger(SPI_USART, &initprs);

	/** pin init **/
	/* Set up CLK pin */
	USART1->ROUTELOC0	 = (USART1->ROUTELOC0 & (~_USART_ROUTELOC0_CLKLOC_MASK)) | USART_ROUTELOC0_CLKLOC_LOC14;
	USART1->ROUTEPEN	  = USART1->ROUTEPEN | USART_ROUTEPEN_CLKPEN;

	/* Disable CS pin */
	USART1->ROUTELOC0	 = (USART1->ROUTELOC0 & (~_USART_ROUTELOC0_CSLOC_MASK)) | USART_ROUTELOC0_CSLOC_LOC1;
	USART1->ROUTEPEN	  = USART1->ROUTEPEN & (~USART_ROUTEPEN_CSPEN);

	/* Disable CTS pin */
	USART1->ROUTELOC1	 = (USART1->ROUTELOC1 & (~_USART_ROUTELOC1_CTSLOC_MASK)) | USART_ROUTELOC1_CTSLOC_LOC0;
	USART1->ROUTEPEN	  = USART1->ROUTEPEN & (~USART_ROUTEPEN_CTSPEN);

	/* Disable RTS pin */
	USART1->ROUTELOC1	 = (USART1->ROUTELOC1 & (~_USART_ROUTELOC1_RTSLOC_MASK)) | USART_ROUTELOC1_RTSLOC_LOC0;
	USART1->ROUTEPEN	  = USART1->ROUTEPEN & (~USART_ROUTEPEN_RTSPEN);

	/* Set up RX pin */
	USART1->ROUTELOC0	 = (USART1->ROUTELOC0 & (~_USART_ROUTELOC0_RXLOC_MASK)) | USART_ROUTELOC0_RXLOC_LOC26;
	USART1->ROUTEPEN	  = USART1->ROUTEPEN | USART_ROUTEPEN_RXPEN;

	/* Set up TX pin */
	USART1->ROUTELOC0	 = (USART1->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK)) | USART_ROUTELOC0_TXLOC_LOC26;
	USART1->ROUTEPEN	  = USART1->ROUTEPEN | USART_ROUTEPEN_TXPEN;


	/** misc setup **/
	/* Disable CTS */
	USART1->CTRLX	  = USART1->CTRLX & (~USART_CTRLX_CTSEN);
	/* Set CTS active low */
	USART1->CTRLX	  = USART1->CTRLX & (~USART_CTRLX_CTSINV);
	/* Set RTS active low */
	USART1->CTRLX	  = USART1->CTRLX & (~USART_CTRLX_RTSINV);
	/* Set CS active low */
	USART1->CTRL	  = USART1->CTRL & (~USART_CTRL_CSINV);
	/* Set TX active high */
	USART1->CTRL	  = USART1->CTRL & (~USART_CTRL_TXINV);
	/* Set RX active high */
	USART1->CTRL	  = USART1->CTRL & (~USART_CTRL_RXINV);

	/* finally enable module */
	USART_Enable(USART1, usartEnable);

	/*****************************************
	 * define pin directions
	 ****************************************/

	GPIO_PinModeSet(MOSI_PORT, MOSI_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(MISO_PORT, MISO_PIN, gpioModeInput, 0);
	GPIO_PinModeSet(SCK_PORT, SCK_PIN, gpioModePushPull, 0);

}
