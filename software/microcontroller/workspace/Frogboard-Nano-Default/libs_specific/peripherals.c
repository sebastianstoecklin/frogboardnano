/***************************************************************************//**
 * @file    peripherals.c
 * @author  Sebastian Stoecklin
 * @date    04/13/2017
 *
 * @brief   A basic library to encapsulate control of board peripherals.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "misc.h"
#include "peripherals.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/
void dcdc_init(void) {

	// $[EMU Initialization]
	/* Initialize DCDC regulator */
	EMU_DCDCInit_TypeDef dcdcInit = EMU_DCDCINIT_DEFAULT;

	dcdcInit.powerConfig           = emuPowerConfig_DcdcToDvdd;
	dcdcInit.dcdcMode              = emuDcdcMode_LowNoise;
	dcdcInit.mVout                 = 1800;
	dcdcInit.em01LoadCurrent_mA    = 15;
	dcdcInit.em234LoadCurrent_uA   = 10;
	dcdcInit.maxCurrent_mA         = 200;
	dcdcInit.anaPeripheralPower    = emuDcdcAnaPeripheralPower_AVDD;
	dcdcInit.reverseCurrentControl = 160;

	EMU_DCDCInit(&dcdcInit);

//	/* Do not use DC, connect 1.8 V line to 3.3 V */
//	EMU_DCDCPowerOff();

	/* Initialize Run/Suspend mode */
	EMU_EM01Init_TypeDef em01Init = EMU_EM01INIT_DEFAULT;

	em01Init.vScaleEM01LowPowerVoltageEnable = 0;

	EMU_EM01Init(&em01Init);
	/* Initialize EM2/EM3 mode */
	EMU_EM23Init_TypeDef em23Init = EMU_EM23INIT_DEFAULT;

	em23Init.em23VregFullEn        = 0;
	em23Init.vScaleEM23Voltage     = emuVScaleEM23_FastWakeup;

	EMU_EM23Init(&em23Init);
	/* Initialize EM4H/S mode */
	EMU_EM4Init_TypeDef em4Init = EMU_EM4INIT_DEFAULT;

	em4Init.retainLfrco            = 0;
	em4Init.retainLfxo             = 0;
	em4Init.retainUlfrco           = 0;
	em4Init.em4State               = emuEM4Shutoff;
	em4Init.pinRetentionMode       = emuPinRetentionDisable;
	em4Init.vScaleEM4HVoltage      = emuVScaleEM4H_FastWakeup;

	EMU_EM4Init(&em4Init);
	// [EMU Initialization]$

}


void oscillators_init(void) {

	// $[High Frequency Clock Setup]
	/* Initializing HFXO */
	CMU_HFXOInit_TypeDef hfxoInit = CMU_HFXOINIT_DEFAULT;

	hfxoInit.ctuneSteadyState      = 500;

	CMU_HFXOInit(&hfxoInit);

	/* Setting system HFXO frequency */
	SystemHFXOClockSet(40000000);

	/* Enable HFXO oscillator, and wait for it to be stable */
	CMU_OscillatorEnable(cmuOsc_HFXO, true, true);

	/* Using HFXO as high frequency clock, HFCLK */
	CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);

	/* HFRCO not needed when using HFXO */
	CMU_OscillatorEnable(cmuOsc_HFRCO, false, false);

	/* Set autostart behaviour */
	CMU_HFXOAutostartEnable(0, false, false);

	// [High Frequency Clock Setup]$


	// $[LE clocks enable]
	/* Enable ULFRCO oscillator, and wait for it to be stable */
	//CMU_OscillatorEnable(cmuOsc_ULFRCO, true, true);

	// [LE clocks enable]$

	// $[LFACLK Setup]
	/* LFACLK is disabled */
	// [LFACLK Setup]$

	// $[LFBCLK Setup]
	/* LFBCLK is disabled */
	// [LFBCLK Setup]$

	// $[LFECLK Setup]
	/* LFECLK is disabled */
	// [LFECLK Setup]$

	// $[Peripheral Clock enables]
	/* Enable clock for HF peripherals */
	CMU_ClockEnable(cmuClock_HFPER, true);

	/* Enable clock for ADC0 */
	CMU_ClockEnable(cmuClock_ADC0, true);

	/* Enable clock for CRYOTIMER */
	CMU_ClockEnable(cmuClock_CRYOTIMER, true);

	/* Enable clock for TIMER0 */
	CMU_ClockEnable(cmuClock_TIMER0, true);

	/* Enable clock for USART0 */
	CMU_ClockEnable(cmuClock_USART0, true);

	/* Enable clock for USART1 */
	CMU_ClockEnable(cmuClock_USART1, true);

	/* Enable clock for GPIO by default */
	CMU_ClockEnable(cmuClock_GPIO, true);

	// [Peripheral Clock enables]$

	// $[Clock output]
	/* Disable CLKOUT0 output */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL0_MASK) | CMU_CTRL_CLKOUTSEL0_DISABLED;
	/* Disable CLKOUT1 output */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL1_MASK) | CMU_CTRL_CLKOUTSEL1_DISABLED;

	// [Clock output]$

	// $[CMU_IO]
	/* Disable CLKIN0 pin */
	CMU->ROUTEPEN	  &= ~CMU_ROUTEPEN_CLKIN0PEN;

	/* Disable CLKOUT0 pin */
	CMU->ROUTEPEN	  &= ~CMU_ROUTEPEN_CLKOUT0PEN;

	/* Set up CLKOUT1 pin */
	CMU->ROUTELOC0	 = (CMU->ROUTELOC0 & (~_CMU_ROUTELOC0_CLKOUT1LOC_MASK)) | CMU_ROUTELOC0_CLKOUT1LOC_LOC0;
	CMU->ROUTEPEN	  |= CMU_ROUTEPEN_CLKOUT1PEN;

	/* Configure CLKOUT1 pin as output */
	GPIO_PinModeSet(gpioPortA,0,gpioModePushPull,0);

	// [CMU_IO]$

}


void osc_enable(void) {
	/* Set CLKOUT1 output to HFXOQ */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL1_MASK)
			| CMU_CTRL_CLKOUTSEL1_HFXOQ;

}

void osc_disable(void) {
	/* Disable CLKOUT1 output */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL1_MASK)
			| CMU_CTRL_CLKOUTSEL1_DISABLED;
}

void dac_enable(void) {
	// Enable clock:
	CMU_ClockEnable(cmuClock_VDAC0, true);

	VDAC_Init_TypeDef vdacInit = VDAC_INIT_DEFAULT;
	VDAC_InitChannel_TypeDef vdacChInit = VDAC_INITCHANNEL_DEFAULT;

	/* Set prescaler to get 1 MHz VDAC clock frequency: */
	vdacInit.prescaler = VDAC_PrescaleCalc(1000000, true, 0);
	vdacInit.reference = vdacRefAvdd;
	VDAC_Init(VDAC0, &vdacInit);
	vdacChInit.enable = true;
	VDAC_InitChannel(VDAC0, &vdacChInit, 1);
}


