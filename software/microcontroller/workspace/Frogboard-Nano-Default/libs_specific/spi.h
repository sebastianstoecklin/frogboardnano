/***************************************************************************//**
 * @file    spi.h
 * @author  Sebastian Stoecklin
 * @date    10/12/2018
 *
 * @brief   A basic library setting up the SPI module.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 *
 ******************************************************************************/

#ifndef INC_SPI_H_
#define INC_SPI_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include "em_device.h"
#include "em_usart.h"
#include "em_ldma.h"
#include "em_gpio.h"
#include "config.h"
#include "misc.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define SPI_USART	USART1
#define SPI_CLK		2000000

/******************************************************************************
 * SPI pinout
 * when changing, please also keep in mind to change the ROUTELOC channels
 * according to the GPIO Functionality Table in the datasheet (p. 131)
 *****************************************************************************/
#define MOSI_PORT	gpioPortF
#define MOSI_PIN	2
#define MISO_PORT	gpioPortF
#define MISO_PIN	3
#define SCK_PORT	gpioPortC
#define SCK_PIN		11

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void spi_init(void);

#endif /* INC_SPI_H_ */
