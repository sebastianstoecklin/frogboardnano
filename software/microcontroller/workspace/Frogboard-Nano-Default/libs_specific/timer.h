/***************************************************************************//**
 * @file    timer.h
 * @author  Sebastian Stoecklin
 * @date    06/27/2017
 *
 * @brief   Just a timer lib.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef INC_TIMER_H_
#define INC_TIMER_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "config.h"
#include "em_device.h"
#include "em_timer.h"
#include "em_gpio.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/
extern volatile uint32_t tick;
/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void TIMER0_IRQHandler(void);
extern void timer_init(uint32_t, void *);
uint32_t timer_getTick(void);
extern void wait_us(uint32_t);
extern void wait_ms(uint32_t);

#endif /* INC_TIMER_H_ */
