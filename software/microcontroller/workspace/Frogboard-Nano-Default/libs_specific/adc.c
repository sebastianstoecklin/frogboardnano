/***************************************************************************//**
 * @file    adc.c
 * @author  Sebastian Stoecklin
 * @date    06/29/2017
 *
 * @brief   A simple library handling basic functions of the ADC.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "adc.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

ADC_Init_TypeDef ADC0_init = ADC_INIT_DEFAULT;
ADC_InitSingle_TypeDef ADC0_init_single = ADC_SINGLE_INIT_WIPOREADER;
ADC_InitScan_TypeDef ADC0_init_scan = ADC_SCAN_INIT_WIPOREADER;

uint32_t adcSampleData[ADC_NUMBER_OF_CHANNELS];
uint32_t adcSampleIndex;
Adc_Status_TypeDef adcStatus = adcIdle;

volatile ADC_Handle_t adc = {
		.sampleData = { 0 },
		.sampleIndex = 0,
		.status = adcIdle
	};

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
void (*adcDoneCallback)(void) = NULL;

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

void ADC0_IRQHandler(void) {
	adc.sampleData[adc.sampleIndex++] = ADC0->SCANDATA;
	adc.sampleData[adc.sampleIndex++] = ADC0->SCANDATA;
	adc.sampleData[adc.sampleIndex++] = ADC0->SCANDATA;
	adc.sampleData[adc.sampleIndex++] = ADC0->SCANDATA;
	if (adc.sampleIndex == ADC_NUMBER_OF_CHANNELS) {
		adc.status = adcDone;
		if(adcDoneCallback != NULL) {
			adcDoneCallback();
		}
	}
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void adc_scan_init(void) {

	ADC0_init.ovsRateSel = adcOvsRateSel2;
	ADC0_init.warmUpMode = adcWarmupKeepInStandby;
	ADC0_init.timebase = ADC_TimebaseCalc(0);
	ADC0_init.prescale = ADC_PrescaleCalc(10000000, 0);
	ADC0_init.tailgate = 0;
	ADC0_init.em2ClockConfig = adcEm2Disabled;

	ADC_Init(ADC0, &ADC0_init);

	ADC_ScanInputClear(&ADC0_init_scan);

	/* Add channels to scan group: */
	/* make sure the individual groups do not intersect with respect to the
	 * groups being allowed for the SCANINPUTSEL register,
	 * see AN0021: Analog to Digital Converter(ADC), p. 20 */
	ADC_ScanSingleEndedInputAdd(&ADC0_init_scan, adcScanInputGroup1,
			adcPosSelAPORT3YCH29); // PB13 - Pin 33 - PA_TEMP
	ADC_ScanSingleEndedInputAdd(&ADC0_init_scan, adcScanInputGroup1,
			adcPosSelAPORT3XCH30); // PB14 - Pin 35 - COIL_CURRENT
	ADC_ScanSingleEndedInputAdd(&ADC0_init_scan, adcScanInputGroup2,
			adcPosSelAPORT1XCH10); // PC10 - Pin 47 - PA_CURRENT
	ADC_ScanSingleEndedInputAdd(&ADC0_init_scan, adcScanInputGroup2,
			adcPosSelAPORT1YCH11); // PC11 - Pin 48 - PA_VOLTAGE

	ADC_InitScan(ADC0, &ADC0_init_scan);

	/* Selects Scan Data Valid level(DVL)
	 * Scan IRQ is triggered when number of scan channels have been converted
	 * and their results are available in the SCAN FIFO */
	ADC0->SCANCTRLX |= (ADC_READ_FIFO_COUNT-1) << _ADC_SCANCTRLX_DVL_SHIFT;
	/* Clear the ADC FIFO */
	ADC0->SCANFIFOCLEAR |= ADC_SCANFIFOCLEAR_SCANFIFOCLEAR;

	/* Enable ADC Interrupt when scan result is available */
	NVIC_ClearPendingIRQ(ADC0_IRQn);
	NVIC_EnableIRQ(ADC0_IRQn);

	ADC_IntEnable(ADC0, ADC_IEN_SCAN);

}

void adc_scan_start(void * adcDoneUserCallback) {
	adc.sampleIndex = 0;
	adc.status = adcRunning;
	adcDoneCallback = adcDoneUserCallback;
	/* Clear the ADC FIFO */
	ADC0->SCANFIFOCLEAR |= ADC_SCANFIFOCLEAR_SCANFIFOCLEAR;
	ADC_Start(ADC0, adcStartScan);
}

Adc_Status_TypeDef adc_scan_getStatus(void) {
	return adc.status;
}

uint32_t * adc_scan_getData(void) {
	adc.status = adcIdle;
	return adc.sampleData;
}

void adc_single_init(void) {

	ADC0_init.ovsRateSel = adcOvsRateSel2;
	ADC0_init.warmUpMode = adcWarmupKeepInStandby;
	ADC0_init.timebase = ADC_TimebaseCalc(0);
	ADC0_init.prescale = ADC_PrescaleCalc(10000000, 0);
	ADC0_init.tailgate = 0;
	ADC0_init.em2ClockConfig = adcEm2Disabled;

	ADC_Init(ADC0, &ADC0_init);
	// [ADC0_Init]$

	ADC_InitSingle(ADC0, &ADC0_init_single);
	// [ADC0_InputConfiguration]$
}

void adc_single_setChannel(uint8_t pin) {

	switch (pin) {
	case 23:
		ADC0_init_single.posSel = adcPosSelAPORT3XCH30;
		break; // Pin 23 - PB14
	case 24:
		ADC0_init_single.posSel = adcPosSelAPORT3YCH31;
		break; // Pin 24 - PB15
	default:
		ADC0_init_single.posSel = adcPosSelTEMP;
		break;	//Default: Temperature sensor
	}

	ADC_InitSingle(ADC0, &ADC0_init_single);
}

uint32_t adc_single_read(void) {
	ADC_Start(ADC0, adcStartSingle);
	while (( ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) {
	}
	return ADC_DataSingleGet(ADC0);
}
