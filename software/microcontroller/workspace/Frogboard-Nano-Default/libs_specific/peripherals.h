/***************************************************************************//**
 * @file    peripherals.h
 * @author  Sebastian Stoecklin
 * @date    04/13/2017
 *
 * @brief   A basic library to encapsulate control of board peripherals.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef INC_PERIPHERAL_H_
#define INC_PERIPHERAL_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "em_device.h"
#include "em_chip.h"
#include "em_gpio.h"
#include "em_vdac.h"
#include "em_cmu.h"
#include "em_emu.h"

#include "config.h"
#include "gpioInterrupt.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void dcdc_init(void);
extern void oscillators_init(void);

extern void ldo_enable(void);
extern void ldo_disable(void);
extern void osc_enable(void);
extern void osc_disable(void);
extern void dac_enable(void);
#if (BOARD_TYPE == WIPO_READER) && (BOARD_REVISION == 5)
extern void button_init(GPIOINT_IrqCallbackPtr_t);
#elif (BOARD_TYPE == WIPO_READER) && (BOARD_REVISION == 6)
extern void button_init(GPIOINT_IrqCallbackPtr_t);
#endif

#endif /* INC_PERIPHERAL_H_ */
