/***************************************************************************//**
 * @file    config.h
 * @author  Sebastian Stoecklin
 * @date    N/A
 *
 * @brief   A header file to set global configuration variables
 *          of your circuit.
 ******************************************************************************/

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "misc.h"
#include "em_chip.h"

/******************************************************************************
 * BOARD CONFIG
 *****************************************************************************/

#define BOARD_TYPE			FROGBOARD_NANO
#define BOARD_REVISION		1
#define BOARD_ID			0x1234
#define BOARD_ID_STRING		"1234"
#define BOARD_ADDRESS		"@1234"

#define TICK_TIME_US		1000
#define SERIAL_DEBUG_ENABLE	0

/******************************************************************************
 * RADIO CONFIG
 *****************************************************************************/

#define RAIL_2400_STABLE	0
#define RAIL_2400_STANDARD	1
#define RAIL_2400_FAST		2

#define RAIL_CONFIG			RAIL_2400_FAST
#define	RAIL_PREAMBLE_INT	FALSE

/******************************************************************************
 * BOARD DEPENDENT DEFINITIONS
 *****************************************************************************/

#if ( (BOARD_TYPE == WIPO_READER) && (BOARD_REVISION == 5) )
/* radio variables */
#define RF_PA_VOLTAGE		1800

#elif ((BOARD_TYPE == WIPO_READER) && (BOARD_REVISION == 6))
/* variables for depicting ADC channel vs. pin */
#define BAT_VOLTAGE			7
#define PA_VOLTAGE			48
#define PA_CURRENT			47
#define PA_TEMP				33
#define COIL_VOLTAGE		36
#define COIL_CURRENT		35
/* radio variables */
#define RF_PA_VOLTAGE		3300

#elif (BOARD_TYPE == FROGBOARD_NANO)
/* radio variables */
#define RF_PA_VOLTAGE		1800
#define STATUS_LED_PORT		gpioPortB
#define STATUS_LED_PIN		13

#endif

#endif /* INC_CONFIG_H_ */
