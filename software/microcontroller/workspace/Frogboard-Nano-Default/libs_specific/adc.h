/***************************************************************************//**
 * @file    adc.h
 * @author  Sebastian Stoecklin
 * @date    06/29/2017
 *
 * @brief   A simple library handling basic functions of the ADC.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef INC_ADC_H_
#define INC_ADC_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "em_device.h"
#include "em_adc.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define ADC_NUMBER_OF_CHANNELS		4
#define ADC_READ_FIFO_COUNT			4

typedef enum {
	adcIdle,	// Ready for new conversion
	adcDone,	// Conversion completed, data ready to be read
	adcRunning	// Conversion in progress
} Adc_Status_TypeDef;

/* type definition for radio handle variable: */
typedef struct ADC_Handle {

	uint32_t sampleData[ADC_NUMBER_OF_CHANNELS];
	uint32_t sampleIndex;
	Adc_Status_TypeDef status;

} ADC_Handle_t;

#define ADC_SINGLE_INIT_WIPOREADER                                                 \
{                                                                                 \
  adcPRSSELCh0,              /* PRS ch0 (if enabled). */                          \
  adcAcqTime2,               /* 4 ADC_CLK cycle acquisition time. */              \
  adcRef2V5,        		 /* 2.5V internal reference. */                       \
  adcRes12Bit,               /* 12 bit resolution. */                             \
  adcPosSelTEMP,       		 /* Select temp. sensor as posSel */                  \
  adcNegSelVSS,              /* Select VSS as negSel */                           \
  false,                     /* Single ended input. */                            \
  false,                     /* PRS disabled. */                                  \
  false,                     /* Right adjust. */                                  \
  false,                     /* Deactivate conversion after one scan sequence. */ \
  false,                     /* No EM2 DMA wakeup from single FIFO DVL */         \
  false                      /* Discard new data on full FIFO. */                 \
}

#define ADC_SCAN_INIT_WIPOREADER                                                     \
  {                                                                                  \
    adcPRSSELCh0,            /* PRS ch0 (if enabled). */                             \
    adcAcqTime2,             /* 4 ADC_CLK cycle acquisition time. */                 \
	adcRef2V5,               /* 2.5V internal reference. */                          \
    adcRes12Bit,             /* 12 bit resolution. */                                \
    {                                                                                \
      /* Initialization should match values set by @ref ADC_ScanInputClear() */      \
	  ADC_SCANINPUTSEL_NONE, /* Default ADC inputs */                          \
      0,                     /* Default input mask (all off) */                      \
      _ADC_SCANNEGSEL_RESETVALUE,/* Default negative select for positive ternimal */ \
    },                                                                               \
    false,                   /* Single-ended input. */                               \
    false,                   /* PRS disabled. */                                     \
    false,                   /* Right adjust. */                                     \
    false,                   /* Deactivate conversion after one scan sequence. */    \
    false,                   /* No EM2 DMA wakeup from scan FIFO DVL */              \
    false                    /* Discard new data on full FIFO. */                    \
  }


/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void adc_scan_init(void);
void adc_scan_start(void *);
extern Adc_Status_TypeDef adc_scan_getStatus(void);
extern uint32_t * adc_scan_getData(void);

extern void adc_single_init(void);
extern void adc_single_setChannel(uint8_t pin);
extern uint32_t adc_single_read(void);

#endif /* INC_SERIAL_H_ */
