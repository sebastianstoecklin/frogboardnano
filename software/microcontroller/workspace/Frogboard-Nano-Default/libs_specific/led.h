/***************************************************************************//**
 * @file    led.h
 * @author  Sebastian Stoecklin
 * @date    06/29/2017
 *
 * @brief   A simple library for controlling an RGB LED.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef INC_LED_H_
#define INC_LED_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "em_device.h"
#include "em_gpio.h"

/* Include file for pin definitions: */

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/
#define RED		0
#define GREEN	1
#define BLUE	2
#define YELLOW	3
#define MAGENTA	4
#define CYAN	5
#define WHITE	6
#define OFF		7

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void led_init(GPIO_Port_TypeDef, unsigned int);
extern void led_on(void);
extern void led_off(void);
extern void led_blink(void);

#endif /* INC_LED_H_ */
