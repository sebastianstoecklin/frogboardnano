/***************************************************************************//**
 * @file    serial.c
 * @author  Sebastian Stoecklin
 * @date    04/08/2017
 *
 * @brief   A basic library for sending strings over the serial	interface.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 *
 * @example char string[] = "Kapier ich nicht!";
 *          serial_printLn(string);
 ******************************************************************************/


#include "serial.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/* Receive ring buffer */
volatile int serialRxReadIndex = 0;       	// Index in buffer to be read
volatile int serialRxWriteIndex = 0;       	// Index in buffer to be written to
volatile uint8_t overflowCnt = 0;			// Number of overflows
volatile char serialRxBuffer[RXBUFSIZE];	// Buffer to store data

/* Transmit buffer */
volatile uint8_t serialTxBuffer[TXBUFSIZE];	// Buffer to store data
volatile int serialTxIndex = 0;
volatile bool serialTxBusy = false;

/* The LDMA transfer should be triggered by the USART0 TX data available signal. */
static LDMA_TransferCfg_t usart0TxTransfer =
LDMA_TRANSFER_CFG_PERIPHERAL(ldmaPeripheralSignal_USART0_TXEMPTY);

/* Transfer bytes from memory to USART0 TX FIFO. */
static LDMA_Descriptor_t usart0TxDesc =
LDMA_DESCRIPTOR_SINGLE_M2P_BYTE(serialTxBuffer, 	   // Source (SRAM)
		&USART0->TXDATA,// Destination (USART)
		0);             // Number of bytes transfers

uint8_t serialDebugEnable = SERIAL_DEBUG_ENABLE; // sets debug output by serial interface

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/* UART/LEUART IRQ Handler*/
void USART0_RX_IRQHandler(void) {
	if (USART0->STATUS & USART_STATUS_RXDATAV) {
		/* Store Data */
		serialRxBuffer[serialRxWriteIndex] = USART_Rx(USART0);
		serialRxWriteIndex++;
		if (serialRxWriteIndex == RXBUFSIZE) {
			serialRxWriteIndex = 0;
		}
		/* Check for overflow - flush buffer */
		if (serialRxWriteIndex == serialRxReadIndex) {
			overflowCnt++;
		}
	}
}

/* LDMA IRQ Handler*/
void LDMA_IRQHandler(void) {
	/* clear busy flag */
	serialTxBusy = false;
	/* clear the interrupt flag */
	LDMA_IntClear(USART_DMA_CHANNEL_BIT);
}

void serial_initiateDmaTransfer(void) {

	/* set busy flag */
	serialTxBusy = true;
	/* Start the DMA transfer from memory to USART */
	usart0TxDesc.xfer.xferCnt = serialTxIndex - 1;
	LDMA_StartTransfer(0, &usart0TxTransfer, &usart0TxDesc);

}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void serial_init(void) {

	/*****************************************
	 * Setup of hardware module
	 ****************************************/

	/** general init **/
	USART_InitAsync_TypeDef initasync = USART_INITASYNC_DEFAULT;
	initasync.enable               = usartDisable;
	initasync.baudrate             = SERIAL_BAUDRATE;
	initasync.databits             = usartDatabits8;
	initasync.parity               = usartNoParity;
	initasync.stopbits             = usartStopbits1;
	initasync.oversampling         = usartOVS4;
	#if defined( USART_INPUT_RXPRS ) && defined( USART_CTRL_MVDIS )
	initasync.mvdis                = 0;
	initasync.prsRxEnable          = 0;
	initasync.prsRxCh              = 0;
	#endif
	USART_InitAsync(SERIAL_USART, &initasync);

	/** PRS init **/
	USART_PrsTriggerInit_TypeDef initprs = USART_INITPRSTRIGGER_DEFAULT;
	initprs.rxTriggerEnable        = 0;
	initprs.txTriggerEnable        = 0;
	initprs.prsTriggerChannel      = usartPrsTriggerCh0;
	USART_InitPrsTrigger(SERIAL_USART, &initprs);

	/** pin init **/

	/* Disable CLK pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_CLKLOC_MASK)) | USART_ROUTELOC0_CLKLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_CLKPEN);

	/* Disable CS pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_CSLOC_MASK)) | USART_ROUTELOC0_CSLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_CSPEN);

	/* Disable CTS pin */
	USART0->ROUTELOC1	 = (USART0->ROUTELOC1 & (~_USART_ROUTELOC1_CTSLOC_MASK)) | USART_ROUTELOC1_CTSLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_CTSPEN);

	/* Disable RTS pin */
	USART0->ROUTELOC1	 = (USART0->ROUTELOC1 & (~_USART_ROUTELOC1_RTSLOC_MASK)) | USART_ROUTELOC1_RTSLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_RTSPEN);

	/* Set up RX pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_RXLOC_MASK)) | USART_ROUTELOC0_RXLOC_LOC5;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN | USART_ROUTEPEN_RXPEN;

	/* Set up TX pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK)) | USART_ROUTELOC0_TXLOC_LOC1;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN | USART_ROUTEPEN_TXPEN;


	/** misc setup **/
	/* Disable CTS */
	USART0->CTRLX	  = USART0->CTRLX & (~USART_CTRLX_CTSEN);
	/* Set CTS active low */
	USART0->CTRLX	  = USART0->CTRLX & (~USART_CTRLX_CTSINV);
	/* Set RTS active low */
	USART0->CTRLX	  = USART0->CTRLX & (~USART_CTRLX_RTSINV);
	/* Set CS active low */
	USART0->CTRL	  = USART0->CTRL & (~USART_CTRL_CSINV);
	/* Set TX active high */
	USART0->CTRL	  = USART0->CTRL & (~USART_CTRL_TXINV);
	/* Set RX active high */
	USART0->CTRL	  = USART0->CTRL & (~USART_CTRL_RXINV);

	/* finally enable module */
	USART_Enable(USART0, usartEnable);

	/*****************************************
	 * define pin directions
	 ****************************************/

	GPIO_PinModeSet(UART_TX_PORT, UART_TX_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(UART_RX_PORT, UART_RX_PIN, gpioModeInput, 0);

	/*****************************************
	 * Set up interrupts
	 ****************************************/

	/* Clear previous RX interrupts */
	USART_IntClear(USART0, USART_IF_RXDATAV);
	NVIC_ClearPendingIRQ(USART0_RX_IRQn);

	/* Enable RX interrupts */
	USART_IntEnable(USART0, USART_IF_RXDATAV);
	NVIC_EnableIRQ(USART0_RX_IRQn);

	/*****************************************
	 * Set up LDMA
	 ****************************************/

	/* Initialize the LDMA with default values. */
	LDMA_Init_t init = LDMA_INIT_DEFAULT;
	LDMA_Init(&init);

	/* Enable interrupt */
	LDMA_IntEnable(BIT0);
	NVIC_EnableIRQ(LDMA_IRQn);
}

uint8_t serial_available(void) {
	if (serialRxWriteIndex != serialRxReadIndex) {
		return 1;
	} else {
		return 0;
	}
}

uint8_t serial_checkError(void) {
	return overflowCnt;
}

void serial_flush(void) {
	serialRxReadIndex = serialRxWriteIndex;
	overflowCnt = 0;
}

char serial_read(void) {
	// If the buffer's start is the buffer's end, there's no data (return -1)
	if (serialRxWriteIndex == serialRxReadIndex) {
		return -1;
	}
	// Save the first byte to a temporary variable, move the start-pointer
	char r = serialRxBuffer[serialRxReadIndex++];
	if (serialRxReadIndex == RXBUFSIZE) {
		serialRxReadIndex = 0;
	}
	// and return the stored byte.
	return r;
}

void serial_write(uint8_t data) {
	while (serialTxBusy)
		;
	USART_Tx(USART0, data);
}

void serial_print(char * data) {

	while (serialTxBusy)
		; // wait for last sequence to be transmitted
	serialTxIndex = 0;

	/* copy data to buffer */
	while (data[serialTxIndex] != 0x00) {
		serialTxBuffer[serialTxIndex] = data[serialTxIndex];
		serialTxIndex++;
	}

	serial_initiateDmaTransfer();
}

void serial_printLn(char * data) {

	while (serialTxBusy)
		; // wait for last sequence to be transmitted
	serialTxIndex = 0;

	/* copy data to buffer */
	while (data[serialTxIndex] != 0x00) {
		serialTxBuffer[serialTxIndex] = data[serialTxIndex];
		serialTxIndex++;
	}

	serialTxBuffer[serialTxIndex] = '\r';
	serialTxIndex++;
	serialTxBuffer[serialTxIndex] = '\n';
	serialTxIndex++;

	serial_initiateDmaTransfer();
}

void serial_debug_printLn(char * data) {
	if (serialDebugEnable) {
		// debug printing should not depend on DMA interrupts:
		uint8_t charCnt = 0;
		while (data[charCnt] != 0) {
			serial_write(data[charCnt++]);
		}
		serial_write('\r');
		serial_write('\n');
	}
}

void serial_printInt(int number) {

	char charBuf[20];
	sprintf(charBuf, "%d", number);
	serial_print(charBuf);

}

void serial_printAdvanced(uint32_t * data, int length,
		Serial_Print_TypeDef format, bool newline) {

	while (serialTxBusy)
		; // wait for last sequence to be transmitted

	serialTxIndex = 0;
	int dataIndex = 0;
	for (dataIndex = 0; dataIndex < length; dataIndex++) { // cycle through all data

		int charIndex = 0;
		char charBuf[16];

		/* generate a string in the correct format */
		if (format == HEX_ASCII) {
			sprintf(charBuf, "%x", (int) data[dataIndex]);
			/* write the string to the serialTxBuffer */
			while (charBuf[charIndex] != 0x00) {
				serialTxBuffer[serialTxIndex] = charBuf[charIndex];
				charIndex++;
				serialTxIndex++;
			}
		} else if (format == DEC_ASCII) {
			sprintf(charBuf, "%d", (int) data[dataIndex]);
			/* write the string to the serialTxBuffer */
			while (charBuf[charIndex] != 0x00) {
				serialTxBuffer[serialTxIndex] = charBuf[charIndex];
				charIndex++;
				serialTxIndex++;
			}
		} else if (format == RAW) {
			/* in RAW format, 32 bits will be transmitted in 4 groups of 8 bit */
			for (charIndex = 0; charIndex < 4; charIndex++) {
				serialTxBuffer[serialTxIndex++] = (char) (data[dataIndex]
						>> (8 * (3 - charIndex))) & 0x000000FF;
			}
		}

		/* add separation or termination character */
		if (length - dataIndex == 1) {
			/* last datum was processed, so insert termination char */
			serialTxBuffer[serialTxIndex++] = LINE_TERMINATION;
		} else {
			/* insert separation char */
			serialTxBuffer[serialTxIndex++] = ' ';
		}

		serialTxBuffer[serialTxIndex] = data[serialTxIndex];
	}

	if (newline) {
		serialTxBuffer[serialTxIndex] = '\r';
		serialTxIndex++;
		serialTxBuffer[serialTxIndex] = '\n';
		serialTxIndex++;
	}

	serial_initiateDmaTransfer();
}

