/***************************************************************************//**
 * @file    serial.h
 * @author  Sebastian Stoecklin
 * @date    04/08/2017
 *
 * @brief   A basic library for sending strings over the serial	interface.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 *
 * @example char string[] = "I don't understand!";
 *          serial_printLn(string);
 ******************************************************************************/

#ifndef INC_SERIAL_H_
#define INC_SERIAL_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include "em_device.h"
#include "em_usart.h"
#include "em_ldma.h"
#include "config.h"
#include "misc.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/
#define SERIAL_USART 				USART0

#define SERIAL_BAUDRATE				1000000

#define RXBUFSIZE					4096	// Buffer size for RX
#define TXBUFSIZE					4096	// Buffer size for TX

#define USART_DMA_CHANNEL			0
#define USART_DMA_CHANNEL_BIT		BIT0

#define LINE_TERMINATION			' '

typedef enum {
	HEX_ASCII,
	DEC_ASCII,
	DEC_BINARY,
	RAW
} Serial_Print_TypeDef;

/******************************************************************************
 * UART pinout
 * when changing, please also keep in mind to change the ROUTELOC channels
 * according to the GPIO Functionality Table in the datasheet (p. 131)
 *****************************************************************************/
#define UART_TX_PORT				gpioPortA
#define UART_TX_PIN					1
#define UART_RX_PORT				gpioPortB
#define UART_RX_PIN					11

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void serial_init(void);

extern uint8_t serial_available(void);
extern uint8_t serial_checkError(void);
extern void serial_flush(void);
extern char serial_read(void);
extern void serial_write(uint8_t);
extern void serial_printInt(int);
extern void serial_printAdvanced(uint32_t *, int, Serial_Print_TypeDef, bool);
extern void serial_print(char *);
extern void serial_printLn(char *);
extern void serial_debug_printLn(char *);

#endif /* INC_SERIAL_H_ */
