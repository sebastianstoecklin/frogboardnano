/***************************************************************************//**
 * @file    led.c
 * @author  Sebastian Stoecklin
 * @date    06/29/2017
 *
 * @brief   A simple library for controlling an RGB LED.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "led.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

GPIO_Port_TypeDef ledPort;
unsigned int ledPin;

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/
void led_init(GPIO_Port_TypeDef port, unsigned int pin){
	ledPort = port;
	ledPin = pin;
}

void led_on(void){
	GPIO_PinModeSet(ledPort, ledPin, gpioModePushPull, 1);
}

void led_off(void){
	GPIO_PinModeSet(ledPort, ledPin, gpioModePushPull, 0);
}

void led_blink(void) {

}

