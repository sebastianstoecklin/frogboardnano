/***************************************************************************//**
 * @file    commands.h
 * @author  Sebastian Stoecklin
 * @date    05/04/2018
 *
 * @brief   A command decoder.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef APPS_COMMANDS_H_
#define APPS_COMMANDS_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

/* Own headers */
#include "config.h"
#include "apps.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/


/* command buffer properties */
#define COMMAND_SUBSTRING_LENGTH	32
#define COMMAND_BUFFER_LENGTH		128

/* commands */
#define SUBDEVICE_COILDRIVER	"coilDriver"

/* subcommands */
#define COMMAND_EXAMPLE	"example"

/* type definition for radio handle variable: */
typedef struct command {

	/* input buffer */
	char charBuffer;
	char buffer[COMMAND_BUFFER_LENGTH];
	uint8_t bufferIndex;

	/* status variable */
	char scanning;	// 1 if scanning procedure is active ('@' detected)

	/* command data */
	char deviceAddress[COMMAND_SUBSTRING_LENGTH];
	char subdevice[COMMAND_SUBSTRING_LENGTH];
	char instruction[COMMAND_SUBSTRING_LENGTH];
	char parameter0[COMMAND_SUBSTRING_LENGTH];
	char parameter1[COMMAND_SUBSTRING_LENGTH];
	char parameter2[COMMAND_SUBSTRING_LENGTH];

} command_t;



/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

void app_listenForCommand(void);

#endif /* APPS_COMMANDS_H_ */
