/***************************************************************************//**
 * @file    apps.h
 * @author  Sebastian Stoecklin
 * @date    10/04/2017
 *
 * @brief   A summary of useful applications for the WiPo devices.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#ifndef APPS_APPS_H_
#define APPS_APPS_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Own headers */
#include "config.h"
#include "led.h"
#include "serial.h"
#include "timer.h"
#include "wireless.h"
#include "peripherals.h"
#include "adc.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/



/******************************************************************************
 * LIBRARY VARIABLES
 *****************************************************************************/

extern volatile WireLEss_Handle_t wireless;

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

void app_echo(void);

void app_wirelessTerminal(void);
void app_wirelessTerminalRxCb(void);
void app_wirelessFlood(void);

void app_printMeasurement(void);
void app_test(void);

#endif /* APPS_APPS_H_ */
