/***************************************************************************//**
 * @file    apps.c
 * @author  Sebastian Stoecklin
 * @date    10/04/2017
 *
 * @brief   A summary of useful applications for the WiPo devices.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "apps.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/* radio variables */
uint16_t txDataLength = 0;
uint16_t rxDataLength = 0;

/* for quick'n'dirty debugging: */
volatile uint32_t packetNo = 0;
int16_t rssi = 0;

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

/* A simple app that just echos all serial input. */
void app_echo(void) {
	/* Echo characters: */
	if (serial_available()) {
		serial_write((uint8_t) serial_read());
	}
}

/* An app that forwards messages in-between UART and radio interface. */
void app_wirelessTerminal(void) {

	wireless_init(app_wirelessTerminalRxCb);

	if (serial_available()) {

		/* delay a bit for allowing the buffer to fill */
		wait_ms(2);
		/* Read the serial interface for incoming data and store them into buffer: */
		txDataLength = 0;
		while (serial_available()) {
			wireless.txBuffer[txDataLength++] = serial_read();
		}

		wireless_scheduleTx(txDataLength); // start TX
	}

	if (wireless.rxDataAvailable) {

		/* Print received packages to terminal: */
		uint16_t i;
		rxDataLength = wireless.rxDataCnt;
		// dont't print the first two bytes, as they contain number of transmitted bytes:
		for (i = 0; i < rxDataLength; i++) {
			serial_write(wireless.rxBuffer[i]);
		}
		// if buffer is processed, move to the next buffer element and discard old data:
		wireless_nextRxBuffer();
	}
}

void app_wirelessTerminalRxCb(void) {

}
