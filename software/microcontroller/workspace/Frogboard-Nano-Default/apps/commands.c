/***************************************************************************//**
 * @file    commands.h
 * @author  Sebastian Stoecklin
 * @date    05/04/2018
 *
 * @brief   A command decoder.
 *
 * @note    The implementation is based on Silabs MCU SDK 5.6.0 and FLEX 2.4.
 ******************************************************************************/

#include "commands.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/* command decoder variables */
command_t command = {
	.bufferIndex = 0,
	.charBuffer = 0,
};

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
void parseCommand(uint8_t);
void executeCommand(void);

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

void parseCommand(uint8_t separator) {
	uint8_t i;
	uint8_t fragmentCount = 0;
	uint8_t fragmentCharCount = 0;
	for (i = 0; i <= command.bufferIndex; i++) {
		if (command.buffer[i] == separator) {
			// command fragment complete
			switch(fragmentCount) {
				case 0:
					command.deviceAddress[fragmentCharCount] = 0;
					break;
				case 1:
					command.subdevice[fragmentCharCount] = 0;
					break;
				case 2:
					command.instruction[fragmentCharCount] = 0;
					break;
				case 3:
					command.parameter0[fragmentCharCount] = 0;
					break;
				case 4:
					command.parameter1[fragmentCharCount] = 0;
					break;
				case 5:
					command.parameter2[fragmentCharCount] = 0;
					break;
				default:
					break;
			}
			fragmentCount++;						// increase fragment count
			fragmentCharCount = 0;		// reset charCounter for next fragment
		} else {
			// add character to currently selected fragment:
			switch(fragmentCount) {
				case 0:
					command.deviceAddress[fragmentCharCount++] = command.buffer[i];
					break;
				case 1:
					command.subdevice[fragmentCharCount++] = command.buffer[i];
					break;
				case 2:
					command.instruction[fragmentCharCount++] = command.buffer[i];
					break;
				case 3:
					command.parameter0[fragmentCharCount++] = command.buffer[i];
					break;
				case 4:
					command.parameter1[fragmentCharCount++] = command.buffer[i];
					break;
				case 5:
					command.parameter2[fragmentCharCount++] = command.buffer[i];
					break;
				default:
					break;
			}
		}
	}
}

void executeCommand(void) {
	if (strcmp(command.deviceAddress, BOARD_ID_STRING) == 0) { //

		/* if address corresponds to own ID: select subdevice: */
		if (strcmp(command.subdevice, SUBDEVICE_COILDRIVER) == 0) {

			/* coilDriver: */
			if (strcmp(command.instruction, COMMAND_EXAMPLE) == 0) {
				// perform the command
			}
		}
	}
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

/* Listens for Command of the structure "@abcd command subcommand parameter0s;"
 * The string is started by the ASCII-character '@' and terminated by ';'.
 * The characters 'abcd' represent the two byte hexadecimal address of the device,
 * being ASCII encoded. The address of a PC based controller is 0000.
 * As soon as '@' is detected, the string is parsed until being terminated by ';' */
void app_listenForCommand(void) {

	if (serial_available() && !command.scanning) {
		// read serial data if available and reading process was not previously started
		command.charBuffer = serial_read();

		if (command.charBuffer == '@') {
			// beginning of command string detected
			command.scanning = 1;
		}
	}

	while (serial_available() && command.scanning) {
		command.charBuffer = serial_read();
		if (command.charBuffer == ';') {
			// command complete: end reading
			command.scanning = 0;
			// start parsing
			command.buffer[command.bufferIndex] = ' ';
			parseCommand(' ');
			//execute command:
			executeCommand();
			// reset
			command.bufferIndex = 0;
		} else if (command.bufferIndex == (COMMAND_BUFFER_LENGTH - 1)) {
			// buffer full without completion => error, finish reading without parsing
			command.scanning = 0;
		} else {
			// regular reading: append next character
			command.buffer[command.bufferIndex++] = command.charBuffer;
		}
	}
}
