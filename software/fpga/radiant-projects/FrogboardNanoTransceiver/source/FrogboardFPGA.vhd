-----------------------------------------------
-- LED Interface
-----------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity FrogboardFPGA is
	port (
		-- F1-F3 as LVDS input, F2 as output
		dataIn		: inout std_logic;
		dataOut		: out std_logic;
		F8			: out std_logic;
		OSC			: in  std_logic
	);
end FrogboardFPGA;

-- behavioral description
architecture behavioral of FrogboardFPGA is

	signal clk 		: std_logic;
	signal res		: std_logic;
	signal clk_out	: std_logic;
	signal cnt 		: unsigned(1 downto 0);
	signal tx_cnt	: unsigned(3 downto 0);
	signal tx_on	: std_logic;

	begin
		
		-- static signal assignments:
		clk <= OSC;
		res <= '1'; 
		dataOut <= clk_out and tx_on;
		F8 <= not(dataIn);
		
		-- simple clock division by 4:
		-- count to 2 and then toggle the logic level:
		clkDivision: process (clk, res)
		begin
			if (res = '0') then
				clk_out <= '0';
				cnt <= "00";
			else 
				if rising_edge(clk) then
					
					if cnt = "01" then
						clk_out <= not(clk_out);
						cnt <= "00";
					else
						cnt <= (cnt + 1);
					end if;
				end if;
			end if;
		end process;


		txEmulator: process (clk_out, res)
		begin
			if (res = '0') then
				tx_on <= '0';
				tx_cnt <= "0000";
			else 
				if rising_edge(clk_out) then
					
					if tx_cnt = "0111" then
						tx_on <= not(tx_on);
						tx_cnt <= "0000";
					else
						tx_cnt <= (tx_cnt + 1);
					end if;
				end if;
			end if;
		end process;
		
end behavioral;
