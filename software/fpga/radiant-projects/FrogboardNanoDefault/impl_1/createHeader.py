###########################################################
# A simple python program converting 
# a binary FPGA configuration file into a
# a header file being flashed to a microprocessor
#
# Author: Sebastian Stoecklin
# Date:   05/16/2019
#
# Usage: Copy this file into the directory of your
#        binary and call
#        python createHeader.py
#
###########################################################

# open binary file of current directory:
import glob, os
os.chdir("./")
for file in glob.glob("*.bin"):
    binFileName = file
binFile = open(binFileName,"rb+")

# read the file
binaryData = bytearray(binFile.read())

# start writing the header:
headerFile = open("fpgaBinaries.h","w+")
headerFile.write("const unsigned char fpgaBinaries_bin[] = {\n  ")

# write the actual data in hexadecimal notation:
for i in range(len(binaryData)-1):
    headerFile.write(str(hex(binaryData[i])) + ", ")
    if (i % 12 is 0) and (i > 0):
        headerFile.write("\n  ") 

headerFile.write("\n};\n\n")

# write a variable indicating the length of the byte array:
headerFile.write("unsigned int fpgaBinaries_bin_len = ")
headerFile.write(str(len(binaryData)))
headerFile.write(";\n")