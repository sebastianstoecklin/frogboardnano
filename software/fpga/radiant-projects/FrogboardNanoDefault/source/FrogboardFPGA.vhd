-----------------------------------------------
-- LED Interface
-----------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity FrogboardFPGA is
	port (
		F1	: out std_logic;
		F2	: out std_logic;
		F3	: out std_logic;
		F4	: out std_logic;
		F5	: out std_logic;
		F6	: out std_logic;
		F7	: out std_logic;
		F8	: out std_logic;
		OSC : in  std_logic
	);
end FrogboardFPGA;

-- behavioral description
architecture behavioral of FrogboardFPGA is

begin
	
	F1 <= '1';
	F2 <= '0';
	F3 <= '1';
	F4 <= '0';
	F5 <= '1';
	F6 <= '0';
	F7 <= '1';
	F8 <= '0';

end behavioral;
