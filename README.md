# Frogboard Nano

![](docs/Frogboard-Nano-V1-PCB.png)Frogboard Nano is a miniaturized ultra-low power computational unit developed by the Laboratory for Electrical Instrumentation at University of Freiburg, Germany. It is supposed to provide a low-power microcontroller and FPGA together with a configurable 2.4 GHz radio transceiver and a flexible power conditioning circuitry.

Its main components are:

* **Silicon Labs EFR32BG13P532GM32** - A 40 MHz microcontroller with ARM Cortex M4F core, fully reconfigurable I/O pins, fast analog peripherals and integrated 2.4 GHz low-power RF transceiver enabling proprietary radio communication with up to 2 MBit/s, generally supporting Bluetooth and Zigbee.
* **Lattice iCE40UP3K** - A simple and low-power FPGA with a quescient current of only 75 µA and support by Open Source FPGA toolchains. The device is intended to be an SPI slave of the microcontroller and must be programmed by the microcontroller.
* **Linear Technology LTC3130** - A DC/DC converter with wide input range of 2.4 to 25 V and low quescient current of 1.2 µA.
* **Custom RF Energy Harvesting Interface** - Carrying two BAT54LP Schottky diodes in a Greinacher rectifier configuration, the board is ready to be powered from various RF sources, with the main intention of using it as a near field wireless power receiver. With an input current sensor based on a Maxim MAX9634FERS+ and a voltage divider circuit, input current and voltage to the DC/DC converter can be monitored precisely.
* **Custom 2.4 GHz Antenna** - Having small size in mind, the board comes with an integrated Inverted-F antenna optimized by numerical simulation with HFSS and matched to 50 Ohms input impendance.

**Please note:** This is an experimental circuit board for research purposes only and not ready for productive usage. Moreover, keep in mind that this board is not certified with respect to regulatory constraints and that using it to emit radio waves might not be allowed in your country.



## Detailed Hardware Description

### Connectors

Power supply, voltage rails provided by the board as well as microcontroller and FPGA I/O are available on a single 26-pin micro-connector (Molex 502426-26) on the bottom of a board, enabling the integration into various small-scale systems. Every target board connecting to Frogboard Nano must therefore provide the opposite male connector, which is a Molex 502430-26.

The target board's pinout is given in the following (top view):

![](docs/Pinout.png)



Here, the microcontroller and FPGA pins correspond to the following port designators of the components:

**Table 1: Microcontroller pins.**

| Frogboard Pin Name | Microcontroller Port Pin | Physical Microcontroller Package Pin | Alternative Pin Function |
| :----------------: | :----------------------: | :----------------------------------: | :----------------------: |
|         P1         |           PF3            |                  4                   |         SPI MISO         |
|         P2         |           PF2            |                  3                   |         SPI MOSI         |
|         P3         |           PC11           |                  32                  |         SPI SCK          |
|         P4         |           PD14           |                  15                  |            -             |
|         P5         |           PB13           |                  21                  |            -             |
|         P6         |           PB12           |                  20                  |            -             |
|         P7         |           PB11           |                  19                  |            -             |
|         P8         |           PA1            |                  18                  |            -             |
|         -          |           PD13           |                  14                  |        FPGA RESET        |
|         -          |           PD15           |                  16                  |        FPGA CDONE        |
|         -          |           PC10           |                  31                  |         FPGA CS          |
|         -          |           PA0            |                  17                  |           OSC            |
|         IR         |           PB14           |                  23                  |       RECT CURRENT       |
|         UR         |           PB15           |                  24                  |       RECT VOLTAGE       |
|       SWCLK        |           PF0            |                  1                   |          SWCLK           |
|       SWDIO        |           PF1            |                  2                   |          SWDIO           |
|       RESET        |          RESETn          |                  8                   |          RESET           |



**Table 2: FPGA pins.**

| Frogboard Pin Name | FPGA Port | Physical FPGA Package Pin | Alternate Pin Function |
| :----------------: | :-------: | :-----------------------: | :--------------------: |
|         F1         |  IOB_2a   |            D5             |   LVDS input with F3   |
|         F2         |  IOB_0a   |            E5             |                        |
|         F3         |  IOB_3b   |            F5             |   LVDS input with F1   |
|         F4         |  IOB_9b   |            E4             |                        |
|         F5         |  IOB_11b  |            F4             |                        |
|         F6         |  IOT_24a  |            B1             |                        |
|         F7         |  IOT_37a  |            A2             |                        |
|         F8         |  IOT_36b  |            A1             |                        |
|         -          |  IOB_32a  |            F1             |        SPI MISO        |
|         -          |  IOB_33b  |            E1             |        SPI MOSI        |
|         -          |  IOB_34a  |            D1             |        SPI SCK         |
|         -          |  CRESET   |            F3             |       FPGA RESET       |
|         -          |  IOB_12a  |            D3             |       FPGA CDONE       |
|         -          |  IOB_35b  |            C1             |        FPGA CS         |
|         -          |  IOT_47a  |            A4             |          OSC           |



### Power supply

The main DC/DC unit based on a Linear Technology LTC3130 converts the voltage from the external DC supply or from the RF rectifier to a main supply voltage of 3.3 V, which is provided 1) to the corresponding pin on the main header, 2) to the analog domain of the EFR32 controller and 3) to the second DC/DC converter being part of the EFR32. 

The EFR32's internal DC/DC converter efficiently generates a 1.8 V voltage rail, being used for all I/O pins on EFR32 and iCE40 and for the RF unit of the EFR32. Moreover, 1.8 V is available on the main header. It is very important to note that the 1.8 V rail requires the DC/DC converter to be enabled in the code of the microcontroller. If disabled, reprogramming might only be possible if externally pulling the 1.8 V rail to a suitable voltage, e.g. by a jumper wire to the 3.3 V rail.

Finally, a 1.2 V rail is generated from the 1.8 V voltage domain with a low-dropout regulator (LDO), just being used for the FPGA core. As the LDO is not that efficient, the 1.2 V rail is not available to the user on any connector.



## Programming

### Microcontroller - EFR32BG13P

Currently, using the *WiPO Breakout Board* together with the *Custom Programmer V2* is the only supported way to program Frogboard Nano using the standard Serial Wire Debug interface (with pins being SWDIO, SWCLK and RESET). The recommended programming line voltage Vtarget is 1.8 V.

If the device does not program, try to turn around the USB-C connector (the symmetric behavior is implemented within the *Custom Programmer V2*, but does not work for some reason).

For a device with disabled internal DC/DC converter (the one in the EFR32, see chapter 'Power Supply' for additional information), pull the 1.8 V line externally to at least 1.8 V (but not more than 3.3 V) in order to flash the device.

So far, only programming and debugging via *Silicon Labs Simplicity Studio V4* has been tested.



### FPGA - iCE40UP3K

The iCE40 must be reprogrammed externally after every power recycle. This can be done by using the FPGA as an SPI slave of the microcontroller, which can carry the FPGA configuration data in its internal flash memory. A bootloader is already available in the EFR32 default project for Frogboard Nano. It accepts a character array containing the FPGA configuration data, which is by default located within the file fpgaBinaries.h. 

To convert the binary FPGA image obtained from your toolchain to a suitable header file, you can use the following workflow:

1. Copy the script 'createHeader.py' (see software/fpga/tools) into your implementation directory containing the binary file and execute it:

   ```bash
   python createHeader.py
   ```

2. Copy the newly created header file 'fpgaBinaries.h' and replace the one in the microcontroller project (it is located in the subfolder libs_fpga).

3. Compile and upload to the microcontroller. Make sure the function 

   ```c
   fpga_flash(fpgaBinaries_bin, fpgaBinaries_bin_len);
   ```

   is called in your program.

*Deprecated:* To convert the binary FPGA image obtained from your toolchain to a suitable header file, you can use the following workflow:

1. Rename your binary file to 'fpgaBinaries.bin'.

2. Convert the binary to a header, e.g. by utilizing

   ```bash
   xxd -i fpgaBinaries.bin fpgaBinaries.h
   ```

3. Replace the header file 'fpgaBinaries.h' in the microcontroller project (it is located in the subfolder libs_fpga) with the file you just obtained.

4. Open the header and change the type of the char array to '**const** unsigned char' to store it in the microcontroller's flash and not in RAM.

5. Compile and upload to the microcontroller. Make sure your function 

   ```c
   fpga_flash(fpgaBinaries_bin, fpgaBinaries_bin_len);
   ```

   is called in your program.

6. Once the microcontroller calls the fpga_flash function, your FPGA is ready to go.

The FPGA can be programmed with a clock of 20 MHz, taking well below 200 ms for the whole procedure.



## Ecosystem

Currently, the following boards have been optimized to work with Frogboard Nano:

* **WiPO Breakout** - A breakout board with programming capabilities and special features to allow and debug wireless power transfer to the controller board.
* **Custom Programmer V2** - A programmer specifically designed to provide power, a serial communication interface and Serial Wire Programming capabilities over a simple USB Type-C connector.
* **ComBiNE AppShield V2** - A first application shield for the Frogboard Nano, featuring a coil for wireless power transfer, an RF receiver for OOK encoding, an RF backscatter transmitter as well as a ZIF connector to a custom neural probe (yes, a thing to record signals from the brain).



## Background Information

The board has been created by Sebastian Stöcklin at University of Freiburg, Germany, in the context of his PhD projects and his research in the field of low power radio systems, supported by the Fritz-Hüttinger-Stiftung and the BrainLinks-BrainTools Cluster of Excellence (Deutsche Forschungsgemeinschaft).

Due to the variety of contexts in which the board appeared, it might be referenced as *WiPO Receiver* (it is basically a near field wireless power receiver) or as *ComBiNE Headunit* (serving as the main control device of a custom neural read-out system). In principle, Frogboard Nano V1 corresponds to V5 of this series of devices, and has come a long way to reach the current size and integration density.

A few words on the naming convention:

> *Frogboard* was my first custom microcontroller circuit board, which I developed completely from scratch. As a result, I thought it could be a good idea naming this board accordingly. Except from that, it's green, it's small and it's wireless for a few centimeters... 🐸